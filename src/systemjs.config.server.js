/**
 * System configuration for Angular samples
 * Adjust as necessary for your application needs.
 */
(function (global) {
  var vQuery = '?v=1.1.10';
  System.config({
    paths: {
      // paths serve as alias
      // 'npm:': 'https://unpkg.com/'
      'npm:': 'node_prod_modules/'
    },
    // map tells the System loader where to look for things
    map: {
      // our app is within the app folder
      app: 'app',
      'img':'img',

      // angular bundles
      '@angular/core': 'npm:@angular/core.umd.js',
      '@angular/common': 'npm:@angular/common.umd.js',
      '@angular/compiler': 'npm:@angular/compiler.umd.js',
      '@angular/platform-browser': 'npm:@angular/platform-browser.umd.js',
      '@angular/platform-browser-dynamic': 'npm:@angular/platform-browser-dynamic.umd.js',
      '@angular/http': 'npm:@angular/http.umd.js',
      '@angular/router': 'npm:@angular/router.umd.js',
      '@angular/forms': 'npm:@angular/forms.umd.js',
      'ng2-webstorage': 'npm:ng2-webstorage/core.umd.js',
      'angular2-google-maps/core': 'npm:angular2-google-maps/core.umd.js',
      // other libraries
      'rxjs':                      'npm:rxjs',
      'materialize-css': 'npm:materialize-css/materialize.min.js',
      'angular2-materialize': 'npm:angular2-materialize',
      'ng2-pickadate':'npm:ng2-pickadate',
      'angular-froala-wysiwyg': 'npm:angular-froala-wysiwyg/angular-froala-wysiwyg.umd.min.js',
      'ngx-facebook':           'npm:ngx-facebook'
    },
    // packages tells the System loader how to load when no filename and/or no extension
    packages: {
      app: {
        defaultExtension: 'js'+ vQuery
      },
      rxjs: {
        defaultExtension: 'js'
      },
      "angular2-materialize": {"main": "dist/index",defaultExtension:'js'},
      "ng2-pickadate":        {"main":"ng2-pickadate",defaultExtension:'js'},
      "ngx-facebook":         {"main":"index",defaultExtension:'js'}
    }
  });
})(this);
