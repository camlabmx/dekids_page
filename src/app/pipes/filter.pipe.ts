import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
    name: 'filter',
    pure: false
})
export class FilterPipe implements PipeTransform {
    transform(items: any[], search: Object): any {
        if (!items || !search) {
            return items;
        }
        let result:any[] = [];
        for(var key in search){
          let tmp = items.filter(item => item[key].toLowerCase().indexOf(search[key].toLowerCase()) !== -1);
          for(var it in tmp){
            if(!(it in result)){
              result.push(tmp[it]);
            }
          }
        }
        return result;
        // filter items array, items which match and return true will be kept, false will be filtered out
        // return items.filter(item => item.name.toLowerCase().indexOf(search['name'].toLowerCase()) !== -1);
    }
}
