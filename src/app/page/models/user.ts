export class User{
  username:string;
  role:string;
  access_level:string;
  name:string;
  last_name:string;
}
