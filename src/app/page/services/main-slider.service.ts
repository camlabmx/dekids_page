import { Injectable } from '@angular/core';
import { Headers, Http, Response } from '@angular/http';

import { Observable } from 'rxjs/Observable';
import { AppSettings }  from '../../app.settings';
import 'rxjs/add/operator/catch';
import 'rxjs/add/operator/map';

import 'rxjs/add/operator/toPromise';

import { Slide } from '../models/slider';

@Injectable()
export class MainSliderService{
  private url = AppSettings.API_ENDPOINT+'main-slider';
  constructor(private http: Http) { }
  getSlides (section:number): Observable<Slide[]> {
    return this.http.get(this.url+'/'+section)
                    .map(this.extractData)
                    .catch(this.handleError);
  }
  private extractData(res: Response) {
    let body = res.json();
    return body.data || { };
  }

  private handleError(error: any): Promise<any> {
    console.error('An error occurred', error); // for demo purposes only
    return Promise.reject(error.message || error);
  }
}
