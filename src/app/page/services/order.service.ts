import { Injectable } from '@angular/core';
import { HttpClient } from '../../app.utils';
import { Observable } from 'rxjs/Observable';

declare var Materialize:any;

@Injectable()
export class OrderService{
  constructor(
    private http: HttpClient
  ) { }
  public getOrderHistory():Observable<any>{
    return this.http.get('order_history/');
  }
}
