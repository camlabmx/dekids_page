import {Injectable} from '@angular/core';
import { HttpClient } from '../../app.utils';
import { LocalStorageService } from 'ng2-webstorage';
import { Observable } from 'rxjs/Observable';
import { Subject } from 'rxjs/Subject';
import { LoaderService } from '../../loader/loader.service';

declare var Materialize:any;

@Injectable()
export class CartService {

  constructor(
    private http: HttpClient,
    private storage:LocalStorageService,
    private loader:LoaderService
  ) { }

  private subject: Subject<number> = new Subject<number>();

  public setQuantity(q: number) {
    this.subject.next(q);
  }
  public getQuantity(): Observable <number>{
    return this.subject.asObservable();
  }

  public getCart(cart_id:any):Observable<any>{
    return this.http.get('cart/'+cart_id);
  }

  public cartOperation(cart_id:any,defaultShipping:any,operation:any,operator:any){
    Materialize.toast("AGREGANDO AL CARRITO.",3000, 'rounded blue lighten-2');
    return this.http.post('cart_operation/',{'cart_id':cart_id,'shippingCity':defaultShipping,'cartOperation':operation,'operator':operator});
  }

  public pagoFacilCheckout(cart_id:any,payment:any):Observable<any>{
    this.loader.displayLoader(true);
    return this.http.post('cart_checkout/',{'_id':cart_id,'payment':payment});
  }
  public checkout(data:any):Observable<any>{
    this.loader.displayLoader(true);
    return this.http.post('cart_checkout/',data);
  }

  public setDeliveryDate(cart_id:any,date:any){
    Materialize.toast("GUARDANDO FECHA DE ENVÍO",3000, 'rounded blue lighten-2');
    this.http.post('set_delivery/',{'cart_id':cart_id,'date':date}).subscribe(res=>{
      Materialize.toast("FECHA ASIGNADA",3000, 'rounded arcana-orange');
    });
  }

  public openPayCheckout(token:any){
    Materialize.toast("PROCESANDO EL PAGO",3000, 'rounded pink darken-2');
    // console.log("T",token);
    return this.http.post("openpay_checkout/",{"token":token});
  }
  public setQuote(cart_id:string,quote_id:string){
    return this.http.post('set_quote/',{'cart_id':cart_id,'quote_id':quote_id});
  }
}
