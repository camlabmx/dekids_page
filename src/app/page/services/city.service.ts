import { Injectable } from '@angular/core';
import { HttpClient } from '../../app.utils';
import { LocalStorageService } from 'ng2-webstorage';
import { Observable } from 'rxjs/Observable';
import { Subject } from 'rxjs/Subject';

@Injectable()
export class CityService{
  constructor(
    private http: HttpClient,
    private storage:LocalStorageService
  ) { }

  private subject: Subject<any> = new Subject<any>();

  public getCities():Observable<any>{
    return this.http.get('city/')
      .map(this.http.extractParsedData).catch(this.http.handleError);
  }

  public setDefault(city:any){
    this.storage.store('defaultShipping',city);
    this.subject.next(city);
  }
  public trackDefault(): Observable <any>{
    return this.subject.asObservable();
  }
  public getDefault(): any{
    return this.storage.retrieve('defaultShipping');
  }
}
