import { Injectable } from '@angular/core';
// import { Headers, Http, Response } from '@angular/http';
import { AppSettings }  from '../../app.settings';
import { HttpClient } from '../../app.utils';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/catch';
import 'rxjs/add/operator/map';

import 'rxjs/add/operator/toPromise';

@Injectable()
export class ProductService{
  private url = 'products';
  constructor(private http: HttpClient
  ) { }
  getProducts (): Observable<any[]> {
    return this.http.get(this.url+'/');
  }
  getProduct (id: string): Observable<any> {
    return this.http.get(this.url+'/'+id);
  }
}

@Injectable()
export class CartProductService{
  private url = 'cart_products';
  constructor(private http: HttpClient) { }
  getProducts (section:string): Observable<any[]> {
    return this.http.get(this.url+'/'+section);
  }

}
