import { Injectable } from '@angular/core';
import { HttpClient } from '../../app.utils';
import { Observable } from 'rxjs/Observable';

declare var Materialize:any;

@Injectable()
export class ContentService{
  constructor(
    private http: HttpClient
  ) { }
  public getContent(section:string):Observable<any>{
    return this.http.get('content/'+section);
  }
  public getBlogContent(id:number):Observable<any>{
    return this.http.get('blog/'+id);
  }
}
