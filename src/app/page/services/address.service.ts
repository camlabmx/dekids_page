import {Injectable} from '@angular/core';
import { HttpClient } from '../../app.utils';
import { LocalStorageService } from 'ng2-webstorage';
import { Observable } from 'rxjs/Observable';
import { Subject } from 'rxjs/Subject';
import { Router }     from '@angular/router';

declare var Materialize:any;

@Injectable()
export class AddressService {

  constructor(
    private http: HttpClient,
    private storage:LocalStorageService,
    private router:Router
  ) { }

  public getUserAddress():Observable<any>{
    return this.http.get('address/');
  }

  public addressOperation(data:any){
    Materialize.toast("GUARDANDO DIRECCIÓN",3000, 'rounded pink darken-2');
    return this.http.post('address/',data);
  }
}
