import { Injectable } from '@angular/core';
import { Router }     from '@angular/router';
import { HttpClient } from '../../app.utils';
import { LocalStorageService } from 'ng2-webstorage';
import { Observable } from 'rxjs/Observable';
import { Subject } from 'rxjs/Subject';
import {LoaderService } from '../../loader/loader.service';
import { FacebookService, InitParams } from 'ngx-facebook';

@Injectable()
export class AuthService{
  private logged_user: Subject<number> = new Subject<number>();
  constructor(
    private http: HttpClient,
    private storage:LocalStorageService,
    private router:Router,
    private loader:LoaderService
  ) { }
  public setUser(u: any) {
    this.logged_user.next(u);
  }
  public getUser(): Observable <any>{
    return this.logged_user.asObservable();
  }
  login(username:string,pwd:string,cart_id:string){
    this.loader.displayLoader(true);
    this.http.post('tokenize/',{'username':username,'pwd':pwd,'cart_id':cart_id})
      .subscribe(result => {
        this.loader.displayLoader(false);
          this.storage.store("token",result.token);
          this.storage.store("user",result.user);
          this.setUser(result.user);
          if(result.user.role=="client"){
            this.router.navigate(['cart']);
          }else if(result.user.role=="delivery"){
            this.router.navigate(['admin', {outlets: {'admin': ['delivery']}}]);
          }
          else{
            this.router.navigate(['admin']);
            // this.router.navigate(['elixir', {outlets: {'admin': [result.redirect]}}]);
          }
      });
  }
  signup(new_user:any){
    this.loader.displayLoader(true);
    this.http.post('user/',new_user)
      .subscribe(result => {
        this.loader.displayLoader(false);
        this.storage.store("token",result.token);
        this.storage.store("user",result.user);
        if(result.user.role=="client"){
          this.router.navigate(['cart']);
        }else{
          this.router.navigate(['elixir']);
        }
      });
  }
  FbLogin(response:any,data:any,cart_id:string){
    this.storage.store("token",response.authResponse.accessToken);
    this.storage.store("user",data);
    this.http.post('fb_login/',{"response":response,"data":data,"cart_id":cart_id})
      .subscribe(result=>this.router.navigate(['cart']));
  }
  logout(){
    this.storage.clear('user');
    this.storage.clear('token');
    this.storage.clear('apps');
  }

}
