import {Injectable} from '@angular/core';
import { Router } from '@angular/router';
import { Observable } from 'rxjs/Observable';
import { Subject } from 'rxjs/Subject';
import { LocalStorageService } from 'ng2-webstorage';

@Injectable()
export class OrderProductService {
  // private product: Subject<any> = new Subject<any>();
  //
  // public setProduct(p: any) {
  //   this.product.next(p);
  // }
  // public getProduct(): Observable <any>{
  //   return this.product.asObservable();
  // }
  constructor(
    private storage:LocalStorageService,
    private router:Router
  ) { }
  public setProduct(p:any){
    this.storage.store("product",p);
    this.router.navigate(['product']);
  }
  public getProduct(){
    return this.storage.retrieve("product");
  }
}
