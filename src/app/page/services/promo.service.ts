import {Injectable} from '@angular/core';
import { HttpClient } from '../../app.utils';
import { LocalStorageService } from 'ng2-webstorage';
import { Observable } from 'rxjs/Observable';
import { Subject } from 'rxjs/Subject';

declare var Materialize:any;

@Injectable()
export class PromoService {

  constructor(
    private http: HttpClient,
    private storage:LocalStorageService
  ) { }

  private subject: Subject<number> = new Subject<number>();

  public ApplyCode(cart_id:any,code:any){
    Materialize.toast("APLICANDO EL CÓDIGO",3000, 'rounded blue darken-2');
    return this.http.post('apply_code/',{'cart_id':cart_id,'code':code});
  }

}
