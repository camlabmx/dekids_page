import {Injectable} from '@angular/core';
import { HttpClient } from '../../app.utils';
import { LocalStorageService } from 'ng2-webstorage';
import { Observable } from 'rxjs/Observable';

declare var Materialize:any;

@Injectable()
export class QuotationService {

  constructor(
    private http: HttpClient,
    private storage:LocalStorageService
  ) { }

  public getQuotes(id:string):Observable<any>{
    return this.http.get('get_quotes/'+id);
  }

  public quotation(id:string){
    return this.http.post('quotation/',{'cart_id':id});
  }

  public setQuote(cart_id:string,quote_id:string){
    return this.http.post('set_quote/',{'cart_id':cart_id,'quote_id':quote_id});
  }
}
