import { Component,OnInit,Input, EventEmitter } from '@angular/core';
import { LocalStorageService } from 'ng2-webstorage';
import { HttpClient } from '../../app.utils';
import { CityService } from '../services/city.service';
import { AddressService } from '../services/address.service';
import { CartService } from '../services/cart.service';
import { Router }     from '@angular/router';
import { MaterializeAction } from 'angular2-materialize';
import { AppSettings } from 'app/app.settings';
import {MaterializeDirective} from "angular2-materialize";

declare var google:any;
declare var Materialize:any;

@Component({
  moduleId: module.id,
  selector:'ship',
  templateUrl:'../templates/ship.component.html?v='+AppSettings.version,
  styles:[`
    .map{
      height:300px;
    }
    `]
})

export class ShipComponent {
  @Input()
  from='client';
  selected_quote:number;
  selectedOption = "";
  selectOptions:any = [];
  constructor(
    private storage:LocalStorageService,
    private http: HttpClient,
    private cityService:CityService,
    private addressService:AddressService,
    private router:Router,
    private _cart:CartService
  ){}
  modalActions = new EventEmitter<string|MaterializeAction>();
  quoteModalActions = new EventEmitter<string|MaterializeAction>();
  user:any;
  new_address:number=0;
  is_logged:boolean = false;
  address:any={};
  email:string;
  emailconf:string;
  user_address:any=[];
  selectedCity:any=null;
  cities:any;
  checked=true;
  map:any;
  geocoder:any;
  address_modified=false;
  quoteIndexSelected:number=0;
  quotes:any;
  cart = {};
  private delivery_date:any;
  private shipping:any;
  private min_date:Date;
  private picker_ready= true;
  private calendar_params = [
    {
      monthsFull: ['Enero', 'Febrero', 'Marzo', 'Abril', 'Mayo', 'Junio', 'Julio', 'Agosto', 'Septiembre', 'Octubre', 'Noviembre', 'Diciembre'],
      monthsShort: ['Ene', 'Feb', 'Mar', 'Abr', 'May', 'Jun', 'Jul', 'Ago', 'Sep', 'Oct', 'Nov', 'Dic'],
      weekdaysFull: ['Domingo', 'Lunes', 'Martes', 'Miercoles', 'Jueves', 'Viernes', 'Sabado'],
      weekdaysShort: ['Dom', 'Lun', 'Mar', 'Mie', 'Jue', 'Vie', 'Sab'],
      showMonthsShort: false,
      format:'dd/mm/yyyy',
      selectMonths: true,
      min: 0,
      today: '',
      clear: 'Limpiar',
      close: 'Cerrar',
      closeOnSelect: true
    }];

  ngOnInit():void{
    this.map = new google.maps.Map(document.getElementById('map'), {
          zoom: 8,
          center: {lat: 19.435314, lng: -99.14256}
        });
    this.geocoder = new google.maps.Geocoder();
    this.loadFields();
    this.user=this.storage.retrieve('user')
    this.cart = this.storage.retrieve('cart');
    //Get user's address
    if(this.storage.retrieve('user')){
      this.getUserAdress();
      this.is_logged=true;
    }else{
      this.new_address=1;
    }
    if(this.storage.retrieve('defaultShipping')){
      this.selectedCity=this.storage.retrieve('defaultShipping');
      this.address['city'] = this.storage.retrieve('defaultShipping');
    }

    if(this.storage.retrieve('cities')){
      this.getCities();
    }else{
      this.cityService.getCities().subscribe(result=>{
          this.cities = result.cities;
          this.storage.store('cities',result.cities);
        }
      );
    }
    this.cityService.trackDefault().subscribe(
      (ship:any) => {
        this.shipping=ship;
        this.SetMinDate(ship['category']);
      }
    );
    this.shipping=this.cityService.getDefault();
    this.SetMinDate(this.cityService.getDefault()['category']);
    if(this.from=='admin'){
      this.address['no-login']=1;
    }
  }
  private SetMinDate(category:string){
    let now = new Date();
    if(now.getHours()>=12){
      this.calendar_params[0].min+=2;
      this.picker_ready=true;
    }else{
      this.calendar_params[0].min+=1;
      this.picker_ready=true;
    }
    if(category!='MET'){
      this.calendar_params[0].min+=1;
    }
  }
  openModal() {
    this.modalActions.emit({action:"modal",params:['open']});
  }
  closeModal() {
    this.modalActions.emit({action:"modal",params:['close']});
  }
  quoteOpenModal() {
    this.quoteModalActions.emit({action:"modal",params:['open']});
  }
  quoteCloseModal() {
    this.quoteModalActions.emit({action:"modal",params:['close']});
  }

  attemptGeocode(){
    if(this.address['street']){
      if(this.address['street2']){
        if(this.address['zip'] && this.address['zip'].length==5){
          this.geocode(this.map);
        }
      }
    }
  }
  addressModification(){
    this.address_modified=true;
  }
  geocode(map:any):any{
    // let address = document.getElementById('street').value+','+document.getElementById('street2').value+','+this.selectedCity['name'];
    let address = this.address['street']+' '+this.address['number']+','+this.address['street2']+','+this.address['zip'];
    let coords = {};
    this.geocoder.geocode({'address': address}, function(results:any, status:any) {
          if (status === 'OK') {
            coords=results[0].geometry.location;
            map.setCenter(new google.maps.LatLng(results[0].geometry.location.lat(),results[0].geometry.location.lng()));
            document.getElementById("city")['value']=results[0].address_components[4].short_name;
            document.getElementById("lat")['value']=results[0].geometry.location.lat();
            document.getElementById("lng")['value']=results[0].geometry.location.lng();
            document.getElementById("country")['value']=results[0].address_components[5].short_name;
            map.setZoom(17);
            var marker = new google.maps.Marker({
              map: map,
              position: results[0].geometry.location
            });
          } else {
            alert('No pudimos ubicar tu dirección, escribela por favor como lo harías en google maps');
          }
        });
    return coords;
  }

  getCities(){
    this.cities = this.storage.retrieve('cities');
  }

  changeCity(city:any){
    //Update storage defaultShipping
    this.storage.store('defaultShipping',city);
    this.selectedCity=city;
    this.address['city'] = city;
  }

  getUserAdress():void{
    this.http.get('address/')
      .subscribe(result => {
        if(!result.address || result.address.length<1){
          this.new_address=1;
          this.address['is_new']=1;
        }
        if(result.address && result.address.length>0){
          this.address = result.address[0];
          this.attemptGeocode();
        }
      });
  }
  Continue(){
    //check data
    if(this.address_modified){
      this.address['cart_id']=this.storage.retrieve('cart')['_id'];
      if(this.Validity()){
        //Send new address to be stored in back
        this.addressService.addressOperation(this.address).subscribe(result => {
          if(result['cart']){
            Materialize.toast("DIRECCIÓN GUARDADA CORRECTAMENTE",3000, 'rounded light-green darken-2');
            this.storage.store("cart",result['cart']);
          }
          if(result['error'] && result['error']=='user'){
            this.openModal();
          }else{
            if(result['quotes'].length==1){
              this.selected_quote=0;
              this.quotes=result['quotes'];
              this.SetQuote();
            }
            if(result['quotes'].length>1){
              this.quotes=result['quotes'];
              this.quoteOpenModal();
            }else{
              this.router.navigate(['payment']);
            }
          }
        });
      }
    }else{
      this.router.navigate(['payment']);
    }

  }
  Validity(){
    let lat = document.getElementById("lat")['value'];
    let lng = document.getElementById("lng")['value'];
    let city = document.getElementById("city")['value'];
    let country = document.getElementById("country")['value'];
    let error: string[] = [];
    if(!this.address['street']){
      error = error.concat('<br/>El campo "Calle" es necesario');
    }
    if(!this.address['cell']){
      error = error.concat('<br/>El campo "Celular" es necesario');
    }
    if(!this.address['phone']){
      error = error.concat('<br/>El campo "Teléfono" es necesario');
    }
    if(!this.address['number']){
      error = error.concat('<br/>El campo "Número" es necesario');
    }
    if(!this.address['street2']){
      error = error.concat('<br/>El campo "Colonia" es necesario ');
    }
    if(!this.address['zip']){
      error = error.concat('<br/>El campo "C.P." es necesario');
    }
    if(!this.address['crossStreet'] || this.address['crossStreet'].length<3){
      error = error.concat('<br/>El campo "Entre Calles" es necesario');
    }
    if(!this.address['reference'] || this.address['reference'].length<3){
      error = error.concat('<br/>El campo "Referencias" es necesario');
    }
    if(!this.is_logged){
      if(this.address['pwd']!=this.address['pwdconf']){
        error = error.concat('<br/>La contraseña y la confirmación no coinciden');
      }
      if(this.address['email']){
        if(this.address['emailconf']){
          if(this.address['emailconf']!=this.address['email']){
            error = error.concat('<br/>El email y la confirmación no coinciden');
          }
        }else{
          error = error.concat('<br/>Es necesario confirmar el email');
        }
      }else{
          error = error.concat('<br/>El email es necesario');
      }
    }
    if(!lat || !lng){
        error = error.concat("<br/>No pudimos verificar tu domicilio, ingresa tu dirección como lo harías en google maps");
    }
    if(!city){
      error = error.concat('<br/>No pudimos verificar tu ciudad, ingresa tu dirección como lo harías en google maps');
    }
    // if(!country || country!='MX'){
    //   error = error.concat('<br/>Lo sentimos, por el momento no tenemos envíos internacionales');
    // }
    if(error.length>0){
      Materialize.toast(error+'',3000, 'rounded red darken-2');
      return false;
    }else{
      this.address['coords'] = {'lat':lat,'lng':lng};
      this.address['city']=city;
      return true;
    }
  }
  CheckoutNoLogin(){
    this.address['no-login']=1;
    this.closeModal();
    this.Continue();
  }
  SetDelivery(date:any){
    this._cart.setDeliveryDate(this.cart['_id'],this.delivery_date);
  }
  public SetQuote(){
    this.quoteCloseModal();
    this._cart.setQuote(this.cart['_id'],this.quotes[this.selected_quote]['_id']).subscribe(res=>{
      this.storage.store('cart',res['cart']);
      this.router.navigate(['payment']);
    });
  }
  private loadFields(){
    if(this.storage.retrieve('cart')){
      if(this.storage.retrieve('cart')['user']){
        this.user=this.storage.retrieve('cart')['user'];
      }
      if(this.storage.retrieve('cart')['address']){
        this.address=this.storage.retrieve('cart')['address'];
        this.address['town']=this.storage.retrieve('cart')['address']['street3'];
        this.address['country']='México';
        this.address['name']=this.user['name'];
        this.address['last_name']=this.user['last_name'];
        this.address['email']=this.user['email'];
        this.attemptGeocode();
      }
    }
  }
}
