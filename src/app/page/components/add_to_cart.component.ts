import { Component,Input } from '@angular/core';
import { LocalStorageService, SessionStorageService } from 'ng2-webstorage';
import { CartService } from '../services/cart.service';

declare var Materialize: any;

@Component({
  moduleId: module.id,
  selector:'addToCart',
  template: `
    <a class="waves-effect waves-light btn arcana-orange white-text" style="padding: 0 0.4rem"  (click)="setValue()"><i class="material-icons center">add</i></a>
  `
})

export class AddToCartComponent {
  private cart: {}[];
  private insert: {};
  private q=0;
  @Input()
  product: string = '';
  @Input()
  productName: string = '';
  @Input()
  variantCode: string = '';
  @Input()
  variantDesc: string = '';
  @Input()
  variantPrice: number = 0.0;
  @Input()
  mainImage: string = 'https://s3.amazonaws.com/elixirdetox/app/public/spree/product_house/1/original/programas_detox.png';
  @Input()
  quantity: number=1;
  @Input()
  alergy: string = '';
  @Input()
  start_detox: string = '';
  constructor(
    private storage:LocalStorageService,
    private cartService:CartService) {}
  productInCart(): boolean{
      for(let element of this.cart){
        if(element['product']===this.product){
            if(element['variantCode']===this.variantCode){
              element['quantity']+=1;
              return true;
            }
        }
      }
    return false;
  }
  setValue() {
    this.cart = this.storage.retrieve('cart');
    this.insert={
          'product':this.product,
          'productName':this.productName,
          'variantCode':this.variantCode,
          'variantShortDesc':this.variantDesc,
          'subtotal':this.variantPrice,
          'amount':this.variantPrice,
          'mainImage':this.mainImage,
          'quantity':this.quantity*1,
          'alergy':this.alergy,
          'start_detox':this.start_detox
        };
    //Frontend manage cart
    // if(this.cart){
    //   if(!this.productInCart())
    //     this.cart.push(this.insert);
    // }else{
    //   this.cart = [this.insert];
    // }
    // this.storage.store('cart', this.cart);
    if(this.cart && this.cart['_id']){
      this.cartService.cartOperation(this.cart['_id'],this.storage.retrieve('defaultShipping'),this.insert,'+').subscribe(result => {
        this.cart = result.cart;
        Materialize.toast(this.productName+" AGREGADO AL CARRITO.",3000, 'rounded arcana-orange');
        this.storage.store("cart",result.cart);
        this.setQuantity();
      });
    }else{
      this.cartService.cartOperation(0,this.storage.retrieve('defaultShipping'),this.insert,'+').subscribe(result => {
        this.cart = result.cart;
        Materialize.toast(this.productName+" AGREGADO AL CARRITO.",3000, 'rounded arcana-orange');
        this.storage.store("cart",result.cart);
        this.setQuantity();
      });
    }
  }
  setQuantity(){
    this.q=0;
    for(let element of this.cart['cart']){
      this.q=this.q+element['quantity'];
    }
    this.cartService.setQuantity(this.q);
  }
}
