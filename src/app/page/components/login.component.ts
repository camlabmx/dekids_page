import { Component,OnInit }    from '@angular/core';
import { AuthService }  from '../services/auth.service';
import { LocalStorageService } from 'ng2-webstorage';
import { FacebookService, LoginResponse, LoginOptions } from 'ngx-facebook';

declare var $:any;

@Component({
  moduleId: module.id,
  selector:'login',
  templateUrl:'../templates/login.component.html',
})

export class LoginComponent implements OnInit{
  username:string;
  pwd:string;
  signUp=0;
  new_user:any={"role":"client","access_level":0};

  ngOnInit(){
    $('.datepicker').pickadate({
      selectMonths: true, // Creates a dropdown to control month
      selectYears: true, // Creates a dropdown of 15 years to control year
    });
  }

  constructor(private auth:AuthService,
    private storage:LocalStorageService,
    private fb: FacebookService
  ){}

  login(){
    if(this.storage.retrieve('cart')){
        this.auth.login(this.username,this.pwd,this.storage.retrieve('cart')['_id']);
    }else{
        this.auth.login(this.username,this.pwd,null);
    }
  }

  FbLogin(){
    const options: LoginOptions = {
      scope: 'public_profile,email',
      return_scopes: true,
      enable_profile_selector: true
    };
    this.fb.login(options)
      .then((response: LoginResponse) => {
        let cart_id:string=null;
        if(this.storage.retrieve('cart')){
            cart_id=this.storage.retrieve('cart')['_id'];
        }
        if(response['status']=='connected'){
          this.fb.api('/me?fields=email,name').then((data:any)=> {
            // console.log("Response",response);
            // console.log("Data",data);
            this.auth.FbLogin(response,data,cart_id);
          });
        }
      }).catch((error: any) => console.error(error));
  }

  signup(){
    if(this.storage.retrieve('cart')){
        this.new_user['cart_id']=this.storage.retrieve('cart')['_id'];
        this.auth.signup(this.new_user);
    }else{
        this.auth.signup(this.new_user);
    }
  }
  show_signup(status:any){
    this.signUp=status;
  }

}
