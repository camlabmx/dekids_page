import { Component,OnInit,Input } from '@angular/core';
import { Observable } from 'rxjs/Rx';

import { Slide } from '../models/slider';
import { MainSliderService } from '../services/main-slider.service';

@Component({
  moduleId: module.id,
  selector:'main-slider',
  templateUrl:'../templates/main-slider.component.html',

  styles: [`
    .slides_container{
  position:relative;
}
.slide{
  overflow-x:hidden;
  width:100%;
  max-width:100%;
  opacity:0;
  position:absolute;
  top:0;
  left:0;
}
.slide_img{
  top:0;
  left:0;
  width:100%;
}
.active-slide{
  cursor:pointer;
  transition: 0.2s all linear;
  opacity:1;
}
.slide_banner{
  position:absolute;
  top:5vh;
  left:75%;
  background-color:rgba(255,255,255,0);
  color:#FFF;
  font-size: 25px;
  font-weight: 300;
  width:250px;
  height:150px;
  z-index:900;
  border-radius:10px;
  text-transform: uppercase;
}
.mobile_slide_banner{
  position:absolute;
  top:50vh;
  left:5vw;
  background-color:rgba(255,255,255,0.3);
  color:#FFF;
  font-size: 30px;
  text-shadow: 2px 2px 2px #000;
  font-weight: 800;
  width:90vw;
  height:40vh;
  z-index:900;
  border-radius:10px;
  text-transform: uppercase;
}
.slide_banner_active{
  transition:0.8s all linear;
  transform: translateX(0px);
}

    `]
})

export class MainSliderComponent implements OnInit{
  @Input()
  section: number = 1;
  shown_slide=1;
  slides:Slide[];
  ngOnInit(){
    this.getSlides();
    let timer = Observable.timer(0,4000);
    timer.subscribe(t=>{this.nextSlide()});
  }
  constructor(private slidesService: MainSliderService){};
  errorMessage = "Error al consultar los productos";
  getSlides(): void{
    this.slidesService.getSlides(this.section).subscribe(
      slides=>this.slides=slides,
      error =>  this.errorMessage = <any>error
    );
  }
  nextSlide(): void{
    if(this.slides){
      this.shown_slide += 1;
      if(this.shown_slide==this.slides.length){
        this.shown_slide=0;
      }
    }
  }

}
