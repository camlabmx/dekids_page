import { Component } from '@angular/core';

@Component({
  moduleId: module.id,
  selector:'phone',
  template: `
  <div class="fixed-action-btn toolbar hide-on-med-and-up">
    <a class="btn-floating btn-large light-green">
      <i class="large material-icons">phone</i>
    </a>
    <ul>
      <!--li class="waves-effect waves-light"><a href="tel:(55)53585908"><i class="material-icons">phone</i>&nbsp;01 (55) 53 58 59 08</a></li-->
      <li class="waves-effect waves-light"><a href="tel:018000033869"><i class="material-icons">phone</i>&nbsp;01 (800) 00 33 869</a></li>
    </ul>
  </div>
  `
})

export class PhoneComponent {

}
