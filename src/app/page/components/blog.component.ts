import { Component,OnInit } from '@angular/core';
import { ContentService }   from '../services/content.service';
import { PagerService } from '../services/pager.service';
import { ActivatedRoute } from '@angular/router';

@Component({
  moduleId: module.id,
  selector:'blog',
  templateUrl:'../templates/blog.component.html',
  styles:[`
      .blog-menu{

      }
      .blog-menu:hover{
        color:white;
        background-color:#e0e0e0;
        cursor:pointer;
      }
      .blog-title{
        font-family: 'Titillium_weblight';
        line-height: 28px;
        font-size: 28px;
        color: black;
      }
      .blog-title:hover{
        color: #558b2f;
        text-decoration: none;
        transition: 0.2s all linear;
        font-size: 28px;
        font-family: 'Titillium_weblight';

      }
      .title-divider{
        background: #ccc;
        display: inline-table;
        clear: both;
        height: 2px;
        width: 100%;
        margin: 20px 0;
      }
      .blog-date{
        color: #636363;
        font-size: 18px;
        display: table;
        vertical-align: bottom;
        font-family: 'Titillium_webthin';
        padding-top: 5px;
      }
      .tag-text{
        color:#9DCF47;
        font-family:"Titillium_websemibold";
        font-size:12px;
        float:left;
        display:inline-block;
        padding-right:5px;
      }
      .tag-icon{
        background: url("https://www.elixirdetox.com/assets/store/icon_tag.png") no-repeat left 3px;
        height: 29px;
        float: left;
        padding-left: 25px;
      }
    `]

})

export class BlogComponent implements OnInit{
  search={'title':''};
  all_blogs:any = [];
  blogs:any = [];
  recent:any=[];
  show_blog:any=null;
  private page:number=1;
  pager = {};
  tags = ['Salud','Mente','Estabilidad','Felicidad','Nutrición','Nutrióloga','Moni','Tips','Bien estar','Alimentación','Juicing','Jugos','Receta','Beneficios'];
  constructor(
    private contentService:ContentService,
    private pagerService:PagerService,
    private activatedRoute:ActivatedRoute
  ){};
  ngOnInit(){
    this.contentService.getContent('blog').subscribe(
      res=>{
        this.all_blogs = res['data'];
        this.setPage(false);
        this.recent = this.all_blogs.filter((item:any, index:number) => index < 5 );

      }
    )
    this.activatedRoute.params.subscribe(params => {
      if(params['index']){
        this.contentService.getBlogContent(params['index']).subscribe(
          res=>this.show_blog=res['data']
        )
      }
     })

  }


  setPage(page:any) {
    if(page){
      this.page = page;
    }
    if (this.page < 1 || this.page > this.pager['totalPages']) {
        return;
    }
    // get pager object from service
    this.pager = this.pagerService.getPager(this.all_blogs.length, this.page);

    // get current page of items
    this.blogs = this.all_blogs.slice(this.pager['startIndex'], this.pager['endIndex'] + 1);
  }
  sharePinterest(blog:any){
    window.open(
      '//pinterest.com/pin/create/button/?url=https://www.elixirdetox.com/blog'
      +'&media='+blog['image']+'&description='+blog['title']+'<...','_blank', 'width=385, height=200, top=0, left=100'
    )
  }
}
