import { Component,OnInit } from '@angular/core';
import { LocalStorageService} from 'ng2-webstorage';
import { CityService } from '../services/city.service';

@Component({
  moduleId: module.id,
  selector:'shipping-city',
  template: `
  <div class="col s12 input-field white-text " style="padding:0">
    <select [ngModel]="shippingCity" style="padding:0;" (ngModelChange)="changeCity($event)" materialize="material_select" [materializeSelectOptions]="{}">
      <option *ngIf="selectedCity" [ngValue]="selectedCity">{{selectedCity.name}}</option>
      <option *ngFor="let city of cities" [ngValue]="city" >{{city.name}}</option>
    </select>
    <label class="white-text" style="left:0;">Ciudad de envío</label>
  </div>
  `

})

export class ShippingCityComponent implements OnInit{
  shippingCity:any;
  selectedCity:any;
  cities:any;
  ngOnInit():void{
    if(this.storage.retrieve('defaultShipping')){
      this.selectedCity=this.storage.retrieve('defaultShipping');
    }

    if(this.storage.retrieve('cities')){
      this.getCities();
    }else{
      this.cityService.getCities().subscribe(result=>{
          this.cities = result.cities;
          this.storage.store('cities',result.cities);
        }
      );
    }
  }
  constructor(
    private storage: LocalStorageService,
    private cityService:CityService,
  ){};

  getCities(){
    this.cities = this.storage.retrieve('cities');
  }

  changeCity(city:any){
    //Update storage defaultShipping
    this.cityService.setDefault(city);
  }

}
