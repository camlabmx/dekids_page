import { Component,OnInit } from '@angular/core';
import { ContentService }   from '../services/content.service';

@Component({
  moduleId: module.id,
  selector:'faq',
  templateUrl:'../templates/faq.component.html',
  styles:[`
    .sticky_sidebar{
      position: -webkit-sticky;
      position: sticky;
      top: 0;
    }
    `]
})

export class FaqComponent implements OnInit{
  faqs:any=[];
  constructor(
    private contentService:ContentService
  ){}
  ngOnInit(){
    this.contentService.getContent('faq').subscribe(
      res => this.faqs=res['data']
    )
  }
  scroll(element:string){
    document.getElementById(element).scrollIntoView();
    window.scrollBy(0,-200);
  }
}
