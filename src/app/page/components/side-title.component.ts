import { Component,Input } from '@angular/core';

@Component({
  moduleId: module.id,
  selector:'side-title',
  template:`
  <div class="side-title z-depth-2 hide-on-small-only white hide-on-med-and-down">
    <div class="title-text rotate arcana-orange-text hide-on-med-and-down">{{title}}</div>
  </div>
  `,
  styles:[`
    .side-title{
      width:15vw;
      height:95vh;
      top:64px;
      position:fixed;
    }
    .title-text{
      font-size:5rem;
      font-weight:100;
      position:absolute;
      width:600px;
      top:130px;
      left:-200px;
      color: #DD592A;
    }
    `]

})

export class SideTitleComponent {
  @Input()
  title: string = 'CONÓCENOS';
}
