import { Component,OnInit } from '@angular/core';
import { GeolocationService } from '../services/geolocation.service';
import { LocalStorageService } from 'ng2-webstorage';
import { WindowService} from '../services/window.service';

@Component({
  moduleId: module.id,
  selector:'elixir-footer',
  templateUrl:'../templates/footer.component.html',
  styles:[`
    .footer-text{
      color:#FFF;
      font-size: 13px;
    }
    .footer-text:hover{
      color:#375F5F;
    }
    .copyright-links{
      color:#FFF;
    }
    .copyright-links:hover{
      color:#375F5F;
    }
    .socialmedia{
      width:100%;
      text-align: center;
    }
    .copyright{
      color: #9c9c9c;
      font-size: 10px;
    }
    .facebook{
      background:url("https://www.elixirdetox.com/assets/store/facebook.png");
      background-repeat:no-repeat;
    }
    .twitter{
      background:url("https://www.elixirdetox.com/assets/store/twitter.png");
      background-repeat:no-repeat;
    }
    .instagram_footer{
      background:url("https://www.elixirdetox.com/assets/store/instagram_footer.png");
      background-repeat:no-repeat;
    }
    .pinterest{
      background:url("https://www.elixirdetox.com/assets/store/pinterest.png");
      background-repeat:no-repeat;
    }
    .youtube{
      background:url("https://www.elixirdetox.com/assets/store/youtube.png");
      background-repeat:no-repeat;
    }
    .snet{
      margin: 0 0 0 5px;
      height: 28px;
      width: 28px;
      float: left;
      display: block;
    }
    .go-top{
      background: #292929;
      display: block;
      height: 35px;
      width: 100%;
      text-align: center;
    }
    .subir{
      background: url("https://www.elixirdetox.com/assets/store/top_arrow.png") center center no-repeat;
      height: 40px;
      width: 100%;
      transition: 0.2s all linear;
      color: #9c9c9c;
      text-decoration: none;
    }
    .subir span{
      font-family: 'Titillium_websemibold';
      font-size: 12px;
      list-style: none;
      text-transform: uppercase;
      top: 14px;
      position: relative;
    }
    .orange-box{
      width:15vw;
      height:100%;
      position:absolute;
      top:0;
      left:0;
      background-color:#DD592A;
    }
    `]
})

export class FooterComponent implements OnInit{
  private _window:any=null;
  constructor(
    private geopos:GeolocationService,
    private storage:LocalStorageService,
    private windowService:WindowService

  ){this._window = windowService.nativeWindow;};
  coords:any = ':(';
  ngOnInit():void{
    if(!this.storage.retrieve('defaultShipping')){
        // this.coords = this.geopos.getCity();
        this.storage.store('defaultShipping',{'name':'CDMX y Area Met.','category':'MET'});//Temp DEMO
    }
  }
  goTop():void{
    for(let h=this._window.innerHeight;h>=0;h--){
      setTimeout(()=>{this._window.scrollTo(0,h);},400);

    }
  }
}
