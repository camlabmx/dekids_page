import { Component, OnInit,EventEmitter } from '@angular/core';
import { CartProductService } from '../services/product.service';
import {MaterializeAction} from 'angular2-materialize';

@Component({
  moduleId: module.id,
  selector:'kits',
  templateUrl:'../templates/kits.component.html',
  styles: [
    `
    .justify{
      text-align: justify;
      text-justify: inter-word;
    }
    `
  ],
})

export class KitsComponent implements OnInit{
  selected_pack:any;
  ngOnInit():void{
    this.getPacks();
  }
  constructor(private cartProductService: CartProductService){};
  packs:any[];
  errorMessage = "Error al consultar los productos";
  modalActions = new EventEmitter<string|MaterializeAction>();
  getPacks(): void{
    this.cartProductService.getProducts('kits').subscribe(
      res=>this.packs=res['data']
    );
  }
  openModal(pack:any) {
    this.selected_pack=pack;
    this.modalActions.emit({action:"modal",params:['open']});
  }
  closeModal() {
    this.modalActions.emit({action:"modal",params:['close']});
  }
}
