import { Component,Input } from '@angular/core';
import { OrderProductService } from '../services/order-product.service';
@Component({
  moduleId: module.id,
  selector:'select-product',
  template: `
    <a class="waves-effect waves-light btn pink darken-2" (click)="setProduct()">MAS INFO</a>
  `
})

export class SelectProductComponent {
  @Input()
  product: any = {};
  constructor(
    private orderProduct:OrderProductService) {}
  setProduct():void{
    this.orderProduct.setProduct(this.product);
  }
}
