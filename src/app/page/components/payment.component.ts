import { Component,OnInit,EventEmitter,Input } from '@angular/core';
import { MaterializeAction } from 'angular2-materialize';
import { LocalStorageService } from 'ng2-webstorage';
import { CartService } from '../services/cart.service';
import { AddressService } from '../services/address.service';
import { LoaderService } from '../../loader/loader.service';
import { QuotationService } from '../services/quotation.service';
import {AppSettings} from '../../app.settings';
import { Observable } from 'rxjs/Rx';
import {MaterializeDirective} from "angular2-materialize";

declare let paypal:any;
declare let $:any;
declare var Materialize:any;
declare var fbq:any;

@Component({
  moduleId: module.id,
  selector:'payment',
  templateUrl:'../templates/payment.component.html?v='+AppSettings.version,
})

export class PaymentComponent {
  @Input()
  from='client';
  card:any={"name":"","last_name":"","number":"","validTo":"","cvv":""};
  address={"phone":"","cell":"","street":"","street2":"","zip":"","town":"","state":"","country":""};
  user:any={};
  ship_address={};
  payment='card';
  cart = {"cart":[{}],"applied_code":"","subtotal":0,"shipping_cost":0,"amount":0};
  private loaded = true;
  quotes:any=[];
  selectedOption = "";
  selectOptions:any = [];
  modalActions = new EventEmitter<string|MaterializeAction>();

  constructor(
    private addressService:AddressService,
    private cartService:CartService,
    private storage:LocalStorageService,
    private loader:LoaderService,
    private _quotes:QuotationService
  ){}

  ngOnInit(){
    this.loadFields();
    this.loader.displayLoader(false);
    this.cart=this.storage.retrieve('cart');
    if(this.cart['code'] && this.cart['code']=='costco' && this.cart['amount']<1){
      this.payment='costco';
    }
    this.createPaypalButton(this.cartService);
    this.openModal();
    let timer = Observable.timer(0,1000);
    timer.subscribe(t=>{this.paypal_checkout_verify()});
  }

  private createPaypalButton(cartService:any){
    let amount = ''+this.storage.retrieve('cart')['amount'];
    let cart_id = this.storage.retrieve('cart')['_id'];
    let url = AppSettings.API_ENDPOINT+'paypal_checkout/';
    paypal.Button.render({

      env: AppSettings.env, // sandbox | production
      style: {
            label: 'pay',
            size:  'responsive', // small | medium | large | responsive
            shape: 'rect',   // pill | rect
            color: 'black'   // gold | blue | silver | black
        },
      // PayPal Client IDs - replace with your own
      // Create a PayPal app: https://developer.paypal.com/developer/applications/create
      client: {
          sandbox:    'AUj9DZ1L-fr1tSQ5cYv0iJ_dccGwhJyDwgIB6SFyfaqMDpcnHOYCQKw_T_nPDab4uACziDBugl0MelAE',
          production: 'AbUrrurATx4FuZlPh5mh3vIMsR-PSjUH2KKYuNE1Z_vzS8A1BCJR8sFpTfcmjCCkcVEokTF1vmNsFf3e'
      },

      // Show the buyer a 'Pay Now' button in the checkout flow
      commit: true,

      // payment() is called when the button is clicked
      payment: function(data:any, actions:any) {

          // Make a call to the REST api to create the payment
          return actions.payment.create({
              payment: {
                  transactions: [
                      {
                          amount: { total: amount, currency: 'MXN' }
                      }
                  ]
              }
          });
      },

      // onAuthorize() is called when the buyer approves the payment
      onAuthorize: function(data:any, actions:any) {
        // Make a call to the REST api to execute the payment
        return actions.payment.execute().then(function() {
          actions.payment.get().then(function(data:any) {
            url=url+cart_id+'/';
            url=url+'paypal'+'/';
            url=url+data['id']+'/';
            url=url+data['payer']['payer_info']['payer_id']+'/';
            url=url+data['state']+'/';
            paypal.request.post(url, {})
             .then(function (res:any) {
                 document.getElementById("ppstate")['value']='payment_ok';
             });
          });
        });
      }

  }, '#paypal-button-container');
  }
  selectPayment(option:string){
    this.payment=option;
    if(option=='paypal'){
      let timer = Observable.timer(0,1000);
      timer.subscribe(t=>{this.paypal_checkout_verify()});
    }
  }
  checkEvent(){
    if(this.loaded){
      this.loaded=false;
      this.clearFields();
    }else{
      this.loaded=true;
      this.loadFields();
    }
  }
  private clearFields(){
    this.card={"name":''};
    this.address={"phone":"","cell":"","street":"","street2":"","zip":"","town":"","state":"","country":"México"};
  }
  private loadFields(){
    if(this.storage.retrieve('cart')){
      this.user=this.storage.retrieve('cart')['user'];
      this.address=this.storage.retrieve('cart')['address'];
      this.address['town']=this.storage.retrieve('cart')['address']['street3'];
      this.address['country']='México';
      this.card['name']=this.user['name'];
      this.card['last_name']=this.user['last_name'];
      this.card['phone']=this.user['phone'];
      this.card['cell']=this.user['cell'];
    }
  }
  expireDateSlash(){
    if(this.card['validTo'].length==2){
      this.card['validTo']+='/';
    }
  }
  openModal() {
    this.modalActions.emit({action:"modal",params:['open']});
  }
  closeModal() {
    this.modalActions.emit({action:"modal",params:['close']});
  }

  checkout(){
    if(this.Validity()){
      console.log("processing");
      this.cartService.pagoFacilCheckout(this.cart['_id'],{'type':this.payment,'card':this.card,'bill_address':this.address}).subscribe(
        result => {
          this.loader.displayLoader(false);
          this.storage.clear('cart');
          this.checkoutSuccess();
      });
    }
  }
  private checkoutSuccess(){
    if(this.from=='admin'){
      Materialize.toast("ORDEN FINALIZADA",3000, 'rounded light-green darken-2');
    }
    else{
      fbq('track', 'Purchase', {
         value: this.cart['amount'],
         currency: 'MXN',
       });
      this.openModal();
    }
  }
  private paypal_checkout_verify(){
    if(document.getElementById("ppstate")['value']=='payment_ok'){
      this.loader.displayLoader(false);
      this.storage.clear('cart');
      this.openModal();
    }
  }
  private Validity(){
    return true;
  }
}
