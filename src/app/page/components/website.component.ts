import { Component } from '@angular/core';

@Component({
  moduleId: module.id,
  selector:'website',
  templateUrl:'../templates/website.component.html',
  styles: [`
    .top-menu{
      top: 0;
      margin:0;
      padding:0;
    }
  `]
})

export class WebsiteComponent {

}
