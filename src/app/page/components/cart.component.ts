import { Component,OnInit,Input } from '@angular/core';
import { LocalStorageService } from 'ng2-webstorage';
import { CartService } from '../services/cart.service';
import { OrderProductService } from '../services/order-product.service';
import { PromoService } from '../services/promo.service';

declare var Materialize:any;

@Component({
  moduleId: module.id,
  selector:'cart',
  templateUrl:'../templates/cart.component.html',
  styles:[`
    .image-container{
      max-height:220px;
      min-height:220px;
      max-width:140px !important;
      min-width:140px;
    }
    .image-limit{
      height:auto;
      max-height:210px;
      width:auto !important;
      max-width:140px;
      margin-left: auto;
	    margin-right: auto;
    }
    .mobile-image-container{
      padding-top:20px;
      max-height:200px;
      min-height:200px;
      max-width:80px !important;
      min-width:80px;
    }
    .mobile-limit{
      height:auto;
      max-height:200px;
      width:auto !important;
      max-width:80px;
      margin-left: auto;
	    margin-right: auto;
    }
    .no-padding{
      padding:0px;
    }
    .no-margin{
      margin:0px;
    }
    .box{
      border: 1px solid #9e9e9e;
    }
    `]
})

export class CartComponent implements OnInit{
  @Input()
  from = 'client';
  constructor(private storage:LocalStorageService,
              private cartService:CartService,
              private orderProductService:OrderProductService,
              private promoService:PromoService
            ) {}
  cart:any;
  items: any;
  private code = '';
  private total_quantity=0;
  private isLogged=false;
  total = 0.0;
  ngOnInit(): void{
    this.cart = this.storage.retrieve('cart');
    // this.storage.store('cart', this.items);
    if(this.cart){
      this.items = this.cart['cart'];
      if(this.items){
        this.setTotal();
        this.setQuantity(this.cart['cart']);
      }
    }
    if(this.storage.retrieve('user')){
      this.isLogged=true;
    }
  }
  private setTotal(): void{
    this.total=0;
    for(let item of this.items){
      this.total=this.total+(item['variantPrice']*item['quantity']);
    }
  }
  private addTo(index:any): void{
    this.items[index]['quantity']+=1;
    this.cartService.cartOperation(this.cart['_id'],this.storage.retrieve('defaultShipping'),this.cart['cart'][index],'+').subscribe(result => {
      this.cart = result.cart;
      this.setQuantity(this.cart['cart']);
      Materialize.toast(this.items[index]['productName']+" AGREGADO AL CARRITO.",3000, 'rounded arcana-orange');
      this.storage.store("cart",result.cart);
    });
  }
  private removeFrom(index:any): void{
    this.items[index]['quantity']-=1;
    this.cartService.cartOperation(this.cart['_id'],this.storage.retrieve('defaultShipping'),this.cart['cart'][index],'-').subscribe(result => {
      this.cart = result.cart;
      this.setQuantity(this.cart['cart']);
      Materialize.toast(this.items[index]['productName']+" DESCONTADO",3000, 'rounded arcana-orange');
      this.storage.store("cart",result.cart);
    });
  }
  private delete(index:any): void{
    this.cartService.cartOperation(this.cart['_id'],this.storage.retrieve('defaultShipping'),this.cart['cart'][index],'-').subscribe(result => {
      this.cart = result.cart;
      this.setQuantity(this.cart['cart']);
      Materialize.toast(this.items[index]['productName']+" ELIMINADO DEL CARRITO.",3000, 'rounded light-green darken-2');
      this.storage.store("cart",result.cart);
    });;
  }
  private setQuantity(cart:any){
    var q=0;
    for(let element of this.cart['cart']){
      q=q+element['quantity'];
    }
    this.total_quantity=q;
    this.cartService.setQuantity(q);
  }

  private applyCode(){
    this.promoService.ApplyCode(this.cart['_id'],this.code).subscribe(result => {
      Materialize.toast("CÓDIGO APLICADO",3000, 'rounded light-green darken-2');
      this.cart = result.cart;
      this.storage.store("cart",result.cart);
    });
  }
}
