import { Component,Input,OnInit,EventEmitter } from '@angular/core';
import {MaterializeAction} from 'angular2-materialize';
import { CityService } from '../services/city.service';
import { AppSettings } from 'app/app.settings';

@Component({
  moduleId: module.id,
  selector:'show-product',
  templateUrl:'../templates/show-product.component.html?v='+AppSettings.version,
  styles:[`
    .image-size{
      height:auto;
      max-height:210px;
      width:auto !important;
      max-width:100%;
      margin-left: auto;
	    margin-right: auto;
    }
    .product-title{
      line-height:28px;
      color:#333;
      text-transform:uppercase;
      font-size:20px;
      padding-bottom:7px;
    }
    .kit-title{
      color:#9dcf47;
      padding-left:10%;
      font-size:39px;
      text-transform: uppercase;
    }
    .product-main{
      position:relative;
      margin-left:15vw;
    }
    .product-content{
      position:absolute;
      width:100%;
      height:100%;
      top:0;
      left:0;
      padding-top:10%;
    }
    .colageno-bullets{
      list-style-image: url('img/favicon.ico');
    }
  `]
})

export class ShowProductComponent implements OnInit{
  constructor(private cityService:CityService){};
  shipping:any;
  ngOnInit(){
    this.cityService.trackDefault().subscribe(
      (ship:any) => {
        this.shipping=ship;
      }
    );
    this.shipping=this.cityService.getDefault();
    console.log(this.pack);
  }
  @Input()
  pack:any;
  selected_pack:any;
  modalActions = new EventEmitter<string|MaterializeAction>();
  main_image: string = 'https://s3.amazonaws.com/elixirdetox/app/public/spree/product_house/1/original/programas_detox.png';
  openModal(pack:any) {
    this.selected_pack=pack;
    this.modalActions.emit({action:"modal",params:['open']});
  }
  closeModal() {
    this.modalActions.emit({action:"modal",params:['close']});
  }
  sharePinterest(){
    window.open(
      '//pinterest.com/pin/create/button/?url=https://www.elixirdetox.com/'+this.pack['section']
      +'&media='+this.pack['main_image']+'&description='+this.pack['description']+'<...','_blank', 'width=385, height=200, top=0, left=100'
    )
  }
}
