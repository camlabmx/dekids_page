import { Component,OnInit }     from '@angular/core';
import { LocalStorageService }  from 'ng2-webstorage';
import { CartProductService } from '../services/product.service';

declare var $:any;
@Component({
  moduleId: module.id,
  selector:'order-product',
  templateUrl:'../templates/order-product.component.html',
  styles:[`
    .justify{
      text-align: justify;
      text-justify: inter-word;
    }
    `]
})

export class OrderProductComponent implements OnInit {
  ngOnInit():void{
    this.getPacks();
  }
  constructor(private cartProductService: CartProductService){};
  packs:any = [{}];
  errorMessage = "Error al consultar los productos";
  getPacks(): void{
    this.cartProductService.getProducts('kits').subscribe(
      res=>this.packs=res['data']
    );
  }
}
