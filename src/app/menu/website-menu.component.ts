import { Component,Input,OnInit } from '@angular/core';
import { LocalStorageService } from 'ng2-webstorage';
import { FacebookService, InitParams } from 'ngx-facebook';

import { CartService } from '../page/services/cart.service';
import { AuthService } from '../page/services/auth.service';
import { AppSettings } from 'app/app.settings';
declare var $:any;
@Component({
  moduleId: module.id,
  selector:'website-menu',
  templateUrl: './website-menu.component.html?v='+AppSettings.version,
  styles:[
    `
    .button-cart{
      float:right;
      position:relative;
      height:56px;
      margin: 0 9px;
      display:inline-block;
      font-size:12px;
    }
    .hide-menu{
      transition: 2s all linear;
      transform: translateX(-100%);
    }
    .show-menu{
      transition: 0.2s all linear;
      transform: translateX(0px);
    }
    `
  ]
})

export class WebsiteMenuComponent implements OnInit{
  @Input()
  menu: string = 'landing';
  mobile_state='';
  in_cart: number =0;
  logged_user:any = null;
  shop_submenu:boolean=false;
  cart:any=null;
  private cart_items:{}[];
  constructor(
    private cartService: CartService,
    private authService: AuthService,
    private storage:LocalStorageService,
    private fb:FacebookService
  ){}
  ngOnInit(){
    this.cartService.getQuantity().subscribe((quantity: number) => {
         this.in_cart = quantity;
     });
    //  this.authService.getUser().subscribe((user:any)=>{
    //    this.logged_user = user;
    //  })
     this.loadCart();
     this.loadUser();
     let initParams: InitParams = {
        appId: '138409270234573',
        xfbml: true,
        version: 'v2.10'
      };

      this.fb.init(initParams);
      this.fb.getLoginStatus().then((response: any) => {
        this.FbStatusCallback(response);
      }
      );
  }
  private FbStatusCallback(response:any){
    console.log("Login Status",response);
    if(response['status']=='connected'){
      this.fb.api('/me?fields=email,name').then((data:any)=> {
          console.log("Me",data);
      });
    }
  }
  private loadUser(){
    this.logged_user = this.storage.retrieve('user');
  }
  private loadCart():void{
    this.cart = this.storage.retrieve('cart');
    if(this.cart!=null){
      this.in_cart=0;
      for(let element of this.cart['cart']){
        this.in_cart=this.in_cart+element['quantity'];
      }
    }
  }
  showSubmenu($event:any,submenu:any):void{
    if(submenu=='shop')
      this.shop_submenu=true;
  }
  hideSubmenu($event:any,submenu:any):void{
    if(submenu=='shop')
      this.shop_submenu=false;
  }
  logout(){
    this.logged_user=null;
    this.authService.logout();
    this.fb.logout();
  }
  MobileClicked(){
    this.mobile_state='hide';
  }
  ShowMobile(){
    this.mobile_state='show';
  }
}
