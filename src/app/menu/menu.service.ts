import { Injectable } from '@angular/core';
import { HttpClient } from '../app.utils';

import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/catch';
import 'rxjs/add/operator/map';

import 'rxjs/add/operator/toPromise';

@Injectable()
export class MenuService{

  constructor(private http: HttpClient) { }
  getMenu (section: string): Observable<any[]> {
    return this.http.get('menu/'+section);
  }
}
