import { Component,OnInit,Input } from '@angular/core';
import { Menu } from './menu';
import { MenuService } from './menu.service';
import { LocalStorageService} from 'ng2-webstorage';
import { Router }     from '@angular/router';

@Component({
  moduleId: module.id,
  selector:'menu',
  template: `
    <div class="navbar-fixed">
      <nav>
        <div class="nav-wrapper arcana-orange">
          <ul id="nav-mobile" class="left">
            <li [routerLink]="['/admin', {outlets: {'admin': ['apps']}}]">
              <a class="white-text">
                <i class="material-icons">
                  apps
                </i>
              </a>
            </li>
          </ul>
          <a href="#" class="brand-logo" style="padding-top:10px;float:left;"><img src="img/arcana.png"></a>
          <ul id="nav-mobile" class="right hide-on-med-and-down">
            <!--li *ngFor="let m of menu" routerLinkActive="active light-green"><a [routerLink]="['/elixir', {outlets: {'admin': ['products']}}]" >{{m.name}}</a></li-->
            <!--li *ngFor="let m of menu" routerLinkActive="active light-green">
              <a *ngIf="m.target" [routerLink]="['/elixir', {outlets: {'admin': [m.target]}}]" >{{m.name}}</a>
              <a *ngIf="m.icon=='lock'" (click)="logout()">
                <i class="material-icons">
                  {{m.icon}}
                </i>
              </a>
            </li-->
            <li>
              <a class="white-text" (click)="logout()">
                <i class="material-icons">
                  lock
                </i>
              </a>
            </li>
          </ul>
        </div>
      </nav>
    </div>
  `,
  styles:[
    `nav ul li:hover {
       background-color: rgba(255,255,255,0.5);
    }`
  ]
})

export class MenuComponent implements OnInit{
  @Input()
  section: string = 'website';
  ngOnInit():void{
    this.getMenu();
  }
  constructor(
    private menuService: MenuService,
    private storage: LocalStorageService,
    private router:Router
  ){};
  menu:Menu[];
  errorMessage = "Error al consultar el menú";
  getMenu(): void{
    this.menuService.getMenu(this.section).subscribe(
      res=>{
        console.log(res);
        this.menu=res['menu'];
        if(this.section=='admin'){
          this.storage.store("apps",res['menu']);
        }
      }
    );
  }
  logout(){
    if(this.cleanStorage()){
      this.router.navigate(['login']);
    }
  }
  cleanStorage():boolean{
    this.storage.clear('user');
    this.storage.clear('token');
    this.storage.clear('cart');
    return true;
  }
}
