import { NgModule }               from '@angular/core';
import { RouterModule, Routes }   from '@angular/router';

import { LandingComponent }       from './page/components/landing.component';
import { CartComponent }          from './page/components/cart.component';
import { ShipComponent }          from './page/components/ship.component';
import { PaymentComponent }       from './page/components/payment.component';
import { KitsComponent }          from './page/components/kits.component';
import { LoginComponent }         from './page/components/login.component';
import { OrderProductComponent }  from './page/components/order-product.component';
import { AboutComponent }          from './page/components/about.component';
import { FaqComponent }          from './page/components/faq.component';
import { BlogComponent }        from './page/components/blog.component';
import { ManifestComponent }       from './page/components/manifest.component';

const routes: Routes = [
  { path: '',  component: LandingComponent },
  { path: 'products',  component: KitsComponent },
  { path: 'product/:prod',  component: OrderProductComponent },
  { path: 'cart',  component: CartComponent },
  { path: 'shipping',  component: ShipComponent },
  { path: 'payment',  component: PaymentComponent },
  { path: 'login',  component: LoginComponent },
  { path: 'about',  component: AboutComponent },
  { path: 'faq',  component: FaqComponent },
  { path: 'blog',  component: BlogComponent },
  { path: 'blog/:index',  component: BlogComponent },
  { path: 'manifest', component: ManifestComponent},
];

@NgModule({
  imports: [ RouterModule.forRoot(routes, {useHash: false}) ],
  exports: [ RouterModule ]
})

export class AppRoutingModule {}
