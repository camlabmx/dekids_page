import { NgModule }             from '@angular/core';
import { BrowserModule }        from '@angular/platform-browser';
// import { BrowserAnimationsModule }        from '@angular/animations';
import { FormsModule }          from '@angular/forms';
import { RouterModule }         from '@angular/router';
import { HttpModule }           from '@angular/http';
import { Ng2Webstorage }        from 'ng2-webstorage';
import { MaterializeModule }    from 'angular2-materialize';
import { AgmCoreModule }        from 'angular2-google-maps/core';
import { PickadateModule }      from 'ng2-pickadate/ng2-pickadate';
import { FacebookModule }       from 'ngx-facebook';

import { FilterPipe }           from './pipes/filter.pipe';
import { SafeHtmlPipe }           from './pipes/safeHtml.pipe';

import { AppComponent }         from './app.component';
import { MenuComponent }        from './menu/menu.component';

//Web page & ecommerce components
import { LoginComponent }       from './page/components/login.component';
import { WebsiteComponent }     from './page/components/website.component';
import { WebsiteMenuComponent } from './menu/website-menu.component';
import { CartComponent }        from './page/components/cart.component';
import { ShipComponent }        from './page/components/ship.component';
import { PaymentComponent }     from './page/components/payment.component';
import { PhoneComponent }       from './page/components/phone.component';
import { FooterComponent }      from './page/components/footer.component';
import { AddToCartComponent }   from './page/components/add_to_cart.component';
import { LandingComponent }     from './page/components/landing.component';
import { MainSliderComponent }  from './page/components/main-slider.component';
import { KitsComponent }        from './page/components/kits.component';
import { ShowProductComponent }   from './page/components/show-product.component';
import { SelectProductComponent } from './page/components/select-product.component';
import { OrderProductComponent }  from './page/components/order-product.component';
import { AboutComponent }       from './page/components/about.component';
import { FaqComponent }         from './page/components/faq.component';
import { BlogComponent }        from './page/components/blog.component';
import { ShippingCityComponent }       from './page/components/shippingCity.component';
import { SideTitleComponent } from './page/components/side-title.component';
import { ManifestComponent } from './page/components/manifest.component';

//Web page & ecommerce services
import { MainSliderService }    from './page/services/main-slider.service';
import { CartService }          from './page/services/cart.service';
import { OrderProductService }  from './page/services/order-product.service';
import { PromoService }         from './page/services/promo.service';
import { AddressService }       from './page/services/address.service';
import { ContentService }       from './page/services/content.service';
import { WindowService }       from './page/services/window.service';
import { OrderService }       from './page/services/order.service';
import { QuotationService }     from './page/services/quotation.service';


import { HttpService }          from './custom_http.service';
import { HttpClient }           from './app.utils';
import { LoaderService }        from './loader/loader.service';
import { LoaderComponent }        from './loader/loader.component';
import { AuthService }          from './page/services/auth.service';
import { MenuService }          from './menu/menu.service';
import { ProductService,
          CartProductService }  from './page/services/product.service';
import { GeolocationService }   from './page/services/geolocation.service';
import { CityService }          from './page/services/city.service';
import { PagerService } from './page/services/pager.service';

import { AppRoutingModule }     from './app-routing.module';

@NgModule({
  imports:      [ BrowserModule,
                  // BrowserAnimationsModule,
                  FormsModule,
                  AppRoutingModule,
                  HttpModule,
                  Ng2Webstorage,
                  MaterializeModule,
                  AgmCoreModule.forRoot({
                    apiKey: 'AIzaSyA7ZnxEcQ1BP8qPvt_yiUw8duVjtD65KAo'
                  }),
                  FacebookModule.forRoot()
                ],
  declarations: [
                  FilterPipe,
                  SafeHtmlPipe,

                  AppComponent,
                  MenuComponent,
                  LoaderComponent,
                  //Page components
                  LoginComponent,
                  WebsiteComponent,
                  WebsiteMenuComponent,
                  CartComponent,
                  ShipComponent,
                  PaymentComponent,
                  PhoneComponent,
                  FooterComponent,
                  AddToCartComponent,
                  LandingComponent,
                  MainSliderComponent,
                  KitsComponent,
                  ShowProductComponent,
                  SelectProductComponent,
                  OrderProductComponent,
                  AboutComponent,
                  FaqComponent,
                  ShippingCityComponent,
                  BlogComponent,
                  SideTitleComponent,
                  ManifestComponent,
                  ],
  bootstrap:    [ AppComponent ],

  providers:    [
                  HttpService,
                  AuthService,
                  LoaderService,
                  HttpClient,
                  MenuService,
                  ProductService,
                  MainSliderService,
                  CartProductService,
                  CartService,
                  ContentService,
                  GeolocationService,
                  OrderProductService,
                  CityService,
                  PromoService,
                  AddressService,
                  WindowService,
                  OrderService,
                  QuotationService,
                  PagerService
                ]
})



export class AppModule { }
