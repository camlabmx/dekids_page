import {Injectable} from '@angular/core';
import { Headers, Response} from '@angular/http';
import { HttpService } from './custom_http.service';
import { LocalStorageService } from 'ng2-webstorage';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/catch';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/toPromise';
import { Router }     from '@angular/router';
import {LoaderService } from './loader/loader.service';

declare var Materialize: any;

@Injectable()
export class HttpClient{
  constructor(
    private storage:LocalStorageService,
    private http: HttpService,
    private router:Router,
    private loader:LoaderService
  ){}
  private createAuthorizationHeader(headers:Headers){
    let token = this.storage.retrieve('token');
    headers.append('Content-type','Application/json');
    if(token){
      headers.append('Authorization',token);
    }
  }

  public get(url:string):Observable<any>{
    let headers = new Headers();
    this.createAuthorizationHeader(headers);
    return this.http.get(url,{
      headers:headers
    }).map(this.extractData).catch(this.handleError);

  }

  public post(url:string, data:any):Observable<any>{
    let headers = new Headers();
    this.createAuthorizationHeader(headers);
    return this.http.post(url,data,{
      headers: headers
    }).map(this.extractData).catch(this.handleError);
  }

  private extractData(res: Response) {
    // this.loaderService.hide();
    let body = res.json();
    return body;
  }

  public extractParsedData(res: Response) {
    let body = res;
    return body;
  }

  public handleError(error: any): Promise<any> {
    // this.loaderService.hide();
    console.log("ERROR:",error);
    Materialize.toast(error._body,3000, 'rounded red darken-2');
    return Promise.reject(error.message || error);
  }
}

@Injectable()
export class Toast{
  public success(message:any){
      Materialize.toast(message,3000, 'rounded arcana-orange');
  }
  public error(message:any){
    Materialize.toast(message,3000, 'rounded red darken-2');
  }

}
