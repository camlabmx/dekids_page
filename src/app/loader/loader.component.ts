import { Component, OnInit, OnDestroy } from '@angular/core';
import { Subscription } from 'rxjs/Subscription';

import { LoaderService } from './loader.service';

@Component({
    selector: 'loader',
    template:`
    <div [class.loader-hidden]="!show">
      <div class="loader-overlay valign-wrapper">
        <div class="row center">
          <div class="preloader-wrapper big active">
            <div class="spinner-layer spinner-green-only">
              <div class="circle-clipper left">
                <div class="circle"></div>
              </div><div class="gap-patch">
                <div class="circle"></div>
              </div><div class="circle-clipper right">
                <div class="circle"></div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
    `
    ,
    styles:[`
      .loader-hidden {
          visibility: hidden;
      }
      .loader-overlay {
          position: absolute;
          width:100%;
          height:100%;
          background-color:rgba(0,0,0,0.5);
          top:0;
          left: 0;
          opacity: 1;
          z-index: 500000;
      }
      `]
})
export class LoaderComponent  implements OnInit{
    show: boolean;

    constructor(
        private loaderService: LoaderService) {
        this.show = false;
    }

    ngOnInit() {
        this.loaderService.loaderStatus.subscribe((val: boolean) => {
            this.show = val;
        });
    }
}
