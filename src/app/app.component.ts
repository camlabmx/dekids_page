import { Component,OnInit } from '@angular/core';

import { CartService } from './page/services/cart.service';

@Component({
  moduleId: module.id,
  selector:'my-app',
  template: `
  <router-outlet></router-outlet>
  `,
})

export class AppComponent {

}
