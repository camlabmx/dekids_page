/**
 * System configuration for Angular samples
 * Adjust as necessary for your application needs.
 */
(function (global) {
  System.config({
    paths: {
      // paths serve as alias
      'npm:': 'node_modules/'
    },
    // map tells the System loader where to look for things
    map: {
      // our app is within the app folder
      'app': 'app',
      'img': 'img',
      // angular bundles
      '@angular/core':              'npm:@angular/core/bundles/core.umd.js',
      '@angular/common':            'npm:@angular/common/bundles/common.umd.js',
      '@angular/compiler':          'npm:@angular/compiler/bundles/compiler.umd.js',
      '@angular/platform-browser':  'npm:@angular/platform-browser/bundles/platform-browser.umd.js',
      '@angular/platform-browser-dynamic': 'npm:@angular/platform-browser-dynamic/bundles/platform-browser-dynamic.umd.js',
      '@angular/http':              'npm:@angular/http/bundles/http.umd.js',
      '@angular/router':            'npm:@angular/router/bundles/router.umd.js',
      '@angular/forms':             'npm:@angular/forms/bundles/forms.umd.js',
      '@angular/animations/browser': 'npm:@angular/animations/bundles/animations-browser.umd.js',
      'ng2-webstorage':             'node_modules/ng2-webstorage',
      'angular2-google-maps/core':  'node_modules/angular2-google-maps/core/core.umd.js',
      // other libraries
      'rxjs':                       'npm:rxjs',
      'materialize-css':            'node_modules/materialize-css',
      'angular2-materialize':       'node_modules/angular2-materialize',
      'ng2-pickadate':              'npm:ng2-pickadate',
      'angular-froala-wysiwyg': 'npm:angular-froala-wysiwyg/bundles/angular-froala-wysiwyg.umd.js',
      'ngx-facebook':           'npm:ngx-facebook'
    },
    // packages tells the System loader how to load when no filename and/or no extension
    packages: {
      app: {
        defaultExtension: 'js'
      },
      rxjs: {
        defaultExtension: 'js'
      },
      'ng2-webstorage':       { main: 'bundles/core.umd.js', defaultExtension: 'js'},
      "materialize-css":      {"main": "dist/js/materialize"},
      "angular2-materialize": {"main": "dist/index",defaultExtension:'js'},
      "ng2-pickadate":        {"main":"ng2-pickadate",defaultExtension:'js'},
      "ngx-facebook":         {"main":"dist/umd/index",defaultExtension:'js'}
    }
  });
})(this);
